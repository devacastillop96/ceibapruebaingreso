import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:ceiba/presentacion/appstate/app_state.dart';
import 'package:ceiba/presentacion/views/list_user/list_user.dart';
import 'package:flutter/material.dart';
import 'injection_container.dart' as di;

void main() {
  di.initial();
  final initialState = AppState(loadingUsers: true, users: []);
  final Store<AppState> store =
      Store<AppState>(reducer, initialState: initialState);

  runApp(MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({Key? key, required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Ceiba',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
          fontFamily: "Poppins-Regular",
        ),
        home: const ListUser(),
      ),
    );
  }
}
