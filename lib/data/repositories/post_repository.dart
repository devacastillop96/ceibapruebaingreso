import 'package:ceiba/data/datasource/http_remote_post.dart';
import 'package:ceiba/domain/entities/post.dart';
import 'package:ceiba/domain/repositories/ipost_respository.dart';

class PostRepository implements IPostRepository {
  final IHttpRemotePost httpRemotePost;

  PostRepository(this.httpRemotePost);

  @override
  Future<List<Post>> allPost(int userId) async {
    final response = await httpRemotePost
        .get(url: "/posts", params: {"userId": userId.toString()});

    List<Post> posts = [];
    for (Map<String, dynamic> item in response) {
      posts.add(Post.fromJson(item));
    }

    return posts;
  }
}
