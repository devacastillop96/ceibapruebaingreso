import 'package:ceiba/data/datasource/sqlite_local.dart';
import 'package:ceiba/data/datasource/http_remote.dart';
import 'package:ceiba/domain/entities/post.dart';
import 'package:ceiba/domain/entities/user.dart';

class UserRepository {
  static String url = "/users";

  static allUsers({
    required Function then,
    required Function catachError,
  }) async {
    List<User> users = await SqliteLocal.all(User().table);
    if (users.isNotEmpty) {
      then(users);
      return;
    }
    HttpRemote.get(
      url: url,
      then: (response) {
        List<User> users = [];
        for (Map<String, dynamic> item in response) {
          User user = User.fromJson(item);
          users.add(user);
          SqliteLocal.insertUser(user);
        }
        then(users);
      },
      catchError: (error) {
        catachError(error);
      },
    );
  }

  Future<List<Post>> allPost({required int userId}) async {
    final response = await HttpRemote.request(
        url: "/posts", params: {"userId": userId.toString()});

    List<Post> posts = [];
    for (Map<String, dynamic> item in response) {
      posts.add(Post.fromJson(item));
    }

    return posts;
  }
}
