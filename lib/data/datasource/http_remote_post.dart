import 'dart:convert';

import 'package:ceiba/core/errors/exceptions.dart';
import 'package:ceiba/env.dart';
import 'package:http/http.dart' as http;

abstract class IHttpRemotePost {
  Future get({
    required String url,
    Map<String, dynamic>? params,
  });
}

class HttpRemotePost implements IHttpRemotePost {
  @override
  Future get({
    required String url,
    Map<String, dynamic>? params,
  }) async {
    var uri = Uri.https(domain, url, params);
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw ServerException();
    }
  }
}
