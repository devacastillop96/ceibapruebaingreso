import 'package:ceiba/domain/entities/model.dart';
import 'package:ceiba/domain/entities/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqliteLocal {
  static Future<Database> database() async {
    return openDatabase(
      join(await getDatabasesPath(), 'ceiba_database.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, phone TEXT, email TEXT)",
        );
      },
      version: 1,
    );
  }

  static Future<void> insertUser(Model model) async {
    final Database db = await database();

    await db.insert(
      model.table,
      model.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<User>> all(String table) async {
    final Database db = await database();

    final List<Map<String, dynamic>> maps = await db.query(table);

    return List.generate(maps.length, (i) {
      return User.fromJson(maps[i]);
    });
  }

  static Future<void> delete(String table, int id) async {
    final db = await database();

    await db.delete(
      table,
    );
  }
}
