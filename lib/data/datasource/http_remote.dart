import 'dart:convert';

import 'package:ceiba/env.dart';
import 'package:http/http.dart' as http;

class HttpRemote {
  static get({
    required String url,
    required Function then,
    required Function catchError,
    Map<String, dynamic>? params,
  }) async {
    var uri = Uri.https(domain, url, params);

    http.get(uri).then((http.Response response) {
      then(json.decode(response.body));
    }).catchError((error) {
      catchError(error);
    });
  }

  static Future request({
    required String url,
    Map<String, dynamic>? params,
  }) async {
    final uri = Uri.https(domain, url, params);
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      return json.decode(response.body);
    }
    return {"message": "Error"};
  }
}
