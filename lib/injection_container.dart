import 'package:ceiba/data/datasource/http_remote_post.dart';
import 'package:ceiba/data/repositories/post_repository.dart';
import 'package:ceiba/domain/repositories/ipost_respository.dart';
import 'package:ceiba/presentacion/views/details_user/cubit/posts_cubit.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> initial() async {
  await execute();
}

Future<void> execute() async {
  sl.registerFactory(() => PostsCubit(sl()));

  sl.registerLazySingleton<IPostRepository>(() => PostRepository(sl()));

  sl.registerLazySingleton<IHttpRemotePost>(() => HttpRemotePost());
}
