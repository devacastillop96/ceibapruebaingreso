import 'package:flutter/material.dart';

class ListIsEmpty extends StatelessWidget {
  final IconData icon;
  const ListIsEmpty({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Icon(icon, size: 56),
          const Text(
            "List is empty",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
