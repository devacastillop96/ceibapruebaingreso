import 'package:ceiba/core/framework/colors.dart';
import 'package:ceiba/domain/entities/user.dart';
import 'package:ceiba/presentacion/views/details_user/widgets/details_user_view.dart';
import 'package:flutter/material.dart';

class DetailsUserScreen extends StatelessWidget {
  final User user;

  const DetailsUserScreen({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: background,
        body: DetailsUserView(user: user),
      ),
    );
  }
}
