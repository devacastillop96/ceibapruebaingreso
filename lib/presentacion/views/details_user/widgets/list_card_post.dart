import 'package:ceiba/domain/entities/post.dart';
import 'package:ceiba/presentacion/views/details_user/widgets/card_post.dart';
import 'package:flutter/material.dart';

class ListCardPost extends StatelessWidget {
  final List<Post> posts;

  const ListCardPost({Key? key, required this.posts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Publicaciones (" + posts.length.toString() + ")",
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Expanded(
              child: ListView.builder(
                itemCount: posts.length,
                itemBuilder: (context, index) {
                  return CardPost(post: posts[index]);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
