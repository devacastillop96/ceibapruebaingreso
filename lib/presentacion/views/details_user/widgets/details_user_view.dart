import 'package:ceiba/domain/entities/user.dart';
import 'package:ceiba/injection_container.dart';
import 'package:ceiba/presentacion/views/details_user/cubit/posts_cubit.dart';
import 'package:ceiba/presentacion/views/details_user/cubit/posts_state.dart';
import 'package:ceiba/presentacion/views/details_user/widgets/list_card_post.dart';
import 'package:ceiba/presentacion/views/details_user/widgets/top_content.dart';
import 'package:ceiba/presentacion/widgets/listisempty.dart';
import 'package:ceiba/presentacion/widgets/loading_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailsUserView extends StatelessWidget {
  final User user;

  const DetailsUserView({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TopContent(user: user),
        const SizedBox(height: 16),
        BlocProvider<PostsCubit>(
          create: (context) => sl<PostsCubit>(),
          child: BlocBuilder<PostsCubit, PostsState>(
            builder: (context, state) {
              return _bulidContentFromState(context, state);
            },
          ),
        )
      ],
    );
  }

  Widget _bulidContentFromState(BuildContext context, PostsState state) {
    if (state is PostsStateInitial) {
      _getPosts(context);
      return const LoadingIndicator();
    } else if (state is PostsStateLoading) {
      return const LoadingIndicator();
    } else if (state is PostsStateLoaded) {
      return state.posts.isNotEmpty
          ? ListCardPost(posts: state.posts)
          : const ListIsEmpty(icon: Icons.description_outlined);
    } else if (state is PostsStateError) {
      return Column(
        children: [
          GestureDetector(
              onTap: () {
                _getPosts(context);
              },
              child: const Center(
                  child: Text("Error, intentar de nuevo más tarde"))),
        ],
      );
    }

    return const Text("Nothing");
  }

  _getPosts(BuildContext context) {
    final usersCubit = context.read<PostsCubit>();
    usersCubit.getPost(user.id);
  }
}
