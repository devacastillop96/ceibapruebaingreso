import 'package:ceiba/domain/entities/post.dart';
import 'package:equatable/equatable.dart';

abstract class PostsState extends Equatable {
  const PostsState();

  @override
  List<Object> get props => [];
}

class PostsStateInitial extends PostsState {}

class PostsStateLoading extends PostsState {}

class PostsStateLoaded extends PostsState {
  final List<Post> posts;

  const PostsStateLoaded(this.posts);

  @override
  List<Object> get props => [posts];
}

class PostsStateError extends PostsState {
  final String message;

  const PostsStateError(this.message);

  @override
  List<Object> get props => [message];
}
