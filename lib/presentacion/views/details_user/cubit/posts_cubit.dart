import 'package:bloc/bloc.dart';
import 'package:ceiba/domain/repositories/ipost_respository.dart';
import 'package:ceiba/presentacion/views/details_user/cubit/posts_state.dart';

class PostsCubit extends Cubit<PostsState> {
  final IPostRepository postRepository;

  PostsCubit(this.postRepository) : super(PostsStateInitial());

  void getPost(int userId) async {
    emit(PostsStateLoading());
    try {
      final posts = await postRepository.allPost(userId);
      emit(PostsStateLoaded(posts));
    } catch (e) {
      emit(const PostsStateError("Ocurrio un error"));
    }
  }
}
