import 'package:ceiba/core/framework/colors.dart';
import 'package:ceiba/domain/entities/user.dart';
import 'package:ceiba/presentacion/views/list_user/widgets/list_user_view.dart';
import 'package:flutter/material.dart';

class ListUserScreen extends StatelessWidget {
  final Function(String) onChangedSeeker;
  final Function(User) verPublicaiones;

  const ListUserScreen(
      {Key? key, required this.onChangedSeeker, required this.verPublicaiones})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      body: SafeArea(
        child: ListUserView(
          onChangedSeeker: onChangedSeeker,
          verPublicaiones: verPublicaiones,
        ),
      ),
    );
  }
}
