import 'package:ceiba/core/framework/colors.dart';
import 'package:flutter/material.dart';

class SeekerInput extends StatelessWidget {
  final Function(String) onChangedSeeker;
  const SeekerInput({Key? key, required this.onChangedSeeker})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Flexible(
          child: Container(
            height: 48,
            padding: const EdgeInsets.symmetric(horizontal: 12),
            decoration: BoxDecoration(
              color: themePrimaryColor.withOpacity(0.3),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Row(
              children: <Widget>[
                IconButton(
                    icon: const Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    onPressed: () {}),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, bottom: 0.0),
                    child: TextFormField(
                        controller: null,
                        decoration: InputDecoration(
                          hintText: "Buscar usuario...",
                          hintStyle:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                          border: InputBorder.none,
                        ),
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                        onChanged: onChangedSeeker),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
