import 'package:ceiba/core/framework/colors.dart';
import 'package:ceiba/domain/entities/user.dart';
import 'package:flutter/material.dart';

class CardUser extends StatelessWidget {
  final User user;
  final Function(User) verPublicaiones;

  const CardUser({Key? key, required this.user, required this.verPublicaiones})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: const Color(0xFFBDBDBD),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(16),
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFBDBDBD),
            blurRadius: 4,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            user.name,
            style: const TextStyle(
              color: themePrimaryColor,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              const Icon(Icons.phone, color: themePrimaryColor),
              const SizedBox(width: 4),
              Text(user.phone),
            ],
          ),
          const SizedBox(height: 2),
          Row(
            children: [
              const Icon(Icons.email, color: themePrimaryColor),
              const SizedBox(width: 4),
              Text(user.email),
            ],
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: GestureDetector(
              onTap: () {
                verPublicaiones(user);
              },
              child: Container(
                padding: const EdgeInsets.all(8.0),
                child: const Text(
                  "Ver publicaciones",
                  style: TextStyle(
                    color: themePrimaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
