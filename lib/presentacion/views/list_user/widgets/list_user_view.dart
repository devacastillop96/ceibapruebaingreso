import 'package:ceiba/domain/entities/user.dart';
import 'package:ceiba/presentacion/appstate/app_state.dart';
import 'package:ceiba/presentacion/views/list_user/widgets/card_user.dart';
import 'package:ceiba/presentacion/views/list_user/widgets/seeker_input.dart';
import 'package:ceiba/presentacion/views/list_user/widgets/top_content.dart';
import 'package:ceiba/presentacion/widgets/listisempty.dart';
import 'package:ceiba/presentacion/widgets/loading_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ListUserView extends StatelessWidget {
  final Function(String) onChangedSeeker;
  final Function(User) verPublicaiones;

  const ListUserView(
      {Key? key, required this.onChangedSeeker, required this.verPublicaiones})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          const TopContent(),
          SeekerInput(onChangedSeeker: onChangedSeeker),
          const SizedBox(height: 8),
          StoreConnector<AppState, AppState>(
            converter: (store) => store.state,
            builder: (context, state) {
              return _buildFromState(state);
            },
          ),
        ],
      ),
    );
  }

  Widget _buildFromState(AppState state) {
    return state.loadingUsers
        ? const LoadingIndicator()
        : state.users.isNotEmpty
            ? _buildListUserCard(state.users)
            : const ListIsEmpty(icon: Icons.groups_rounded);
  }

  Widget _buildListUserCard(List<User> users) {
    return Expanded(
      child: ListView.builder(
        itemCount: users.length,
        itemBuilder: (context, index) {
          return CardUser(user: users[index], verPublicaiones: verPublicaiones);
        },
      ),
    );
  }
}
