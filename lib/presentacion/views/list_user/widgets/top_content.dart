import 'package:ceiba/core/framework/colors.dart';
import 'package:flutter/material.dart';

class TopContent extends StatelessWidget {
  const TopContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "Prueba de",
                  style: TextStyle(fontSize: 18.5, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Ingreso",
                  style: TextStyle(
                    color: themePrimaryColor,
                    fontSize: 20.5,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  "¡Quiero ser parte de la Familia Ceiba!",
                  style: TextStyle(color: Color(0xFF757575)),
                ),
              ],
            ),
          ),
          Container(
            width: 180,
            height: 120,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/logo.png"),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
