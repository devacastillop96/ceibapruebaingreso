import 'package:ceiba/presentacion/appstate/app_state.dart';
import 'package:ceiba/domain/entities/user.dart';
import 'package:ceiba/presentacion/views/details_user/details_user_screen.dart';
import 'package:ceiba/presentacion/views/list_user/list_user_screen.dart';
import 'package:ceiba/data/repositories/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ListUser extends StatefulWidget {
  const ListUser({Key? key}) : super(key: key);

  @override
  _ListUserState createState() => _ListUserState();
}

class _ListUserState extends State<ListUser> {
  List<User> users = [];

  @override
  void initState() {
    super.initState();
    allUsers();
  }

  void allUsers() {
    UserRepository.allUsers(
      then: (List<User> usersResponse) {
        users = usersResponse;
        StoreProvider.of<AppState>(context).dispatch(
          RefreshUsers(
            loadingUsers: false,
            users: users,
          ),
        );
      },
      catachError: (error) {
        StoreProvider.of<AppState>(context).dispatch(
          RefreshUsers(
            loadingUsers: false,
            users: [],
          ),
        );
      },
    );
  }

  void onChangedSeeker(String cadena) {
    List<User> usersFilter = users
        .where((user) => user.name.toLowerCase().contains(cadena.toLowerCase()))
        .toList();

    StoreProvider.of<AppState>(context).dispatch(
      RefreshUsers(users: usersFilter, loadingUsers: false),
    );
  }

  void verPublicaiones(User user) {
    FocusScope.of(context).requestFocus(FocusNode());
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailsUserScreen(user: user),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListUserScreen(
      onChangedSeeker: onChangedSeeker,
      verPublicaiones: verPublicaiones,
    );
  }
}
