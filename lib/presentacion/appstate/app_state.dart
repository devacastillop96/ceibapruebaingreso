import 'package:ceiba/domain/entities/user.dart';

class AppState {
  late bool loadingUsers;
  late List<User> users;

  AppState({
    required this.loadingUsers,
    required this.users,
  });

  AppState.fromAppState(AppState appState) {
    loadingUsers = appState.loadingUsers;
    users = appState.users;
  }
}

class RefreshUsers {
  bool loadingUsers;
  List<User> users;
  RefreshUsers({required this.loadingUsers, required this.users});
}

AppState reducer(AppState prevAppState, dynamic action) {
  AppState nextState = AppState.fromAppState(prevAppState);
  if (action is RefreshUsers) {
    nextState.users = action.users;
    nextState.loadingUsers = action.loadingUsers;
  }

  return nextState;
}
