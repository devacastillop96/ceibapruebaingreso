class Post {
  late int id;
  late String title;
  late String body;

  Post.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        body = json['body'];
}
