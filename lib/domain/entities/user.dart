import 'package:ceiba/domain/entities/model.dart';
import 'package:ceiba/domain/entities/post.dart';

class User implements Model {
  @override
  String table = "users";

  late int id;
  late String name;
  late String phone;
  late String email;

  List<Post> posts = [];

  User();

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        phone = json['phone'],
        email = json['email'];

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'phone': phone,
        'email': email,
      };
}
