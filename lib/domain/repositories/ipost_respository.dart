import 'package:ceiba/domain/entities/post.dart';

abstract class IPostRepository {
  Future<List<Post>> allPost(int userId);
}
