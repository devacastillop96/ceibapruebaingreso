import 'package:ceiba/domain/entities/user.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('El usuario debe ser mapeado from json', () {
    final user = User.fromJson({
      'id': 1,
      'name': 'Adrian Castillo',
      'phone': '5823098',
      'email': 'example@gmail.com'
    });

    expect(user.id, 1);
    expect(user.name, 'Adrian Castillo');
    expect(user.phone, '5823098');
    expect(user.email, 'example@gmail.com');
  });
}
