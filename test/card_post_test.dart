import 'package:ceiba/domain/entities/post.dart';
import 'package:ceiba/presentacion/views/details_user/widgets/card_post.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Creacion de CardPost', (WidgetTester tester) async {
    final post = Post.fromJson({
      'id': 1,
      'title': 'Adrian Castillo',
      'body': 'Arquitecto de Software'
    });
    await tester.pumpWidget(MaterialApp(home: CardPost(post: post)));

    final titleFinder = find.text('Adrian Castillo');
    final bodyFinder = find.text('Arquitecto de Software');
    expect(titleFinder, findsOneWidget);
    expect(bodyFinder, findsOneWidget);
  });
}
